// In the name of God

// Seyyed Hossein Khatami Bidgoli   9631020
// Alireza Mazochi                  9631064

#include<Keypad.h>
#include<LiquidCrystal.h>
#include<MenuBackend.h>
#include <Servo.h>
#include <DueTimer.h>

// LCD Setup
const int rs = 8, en = 9, d4 = 10, d5 = 11, d6 = 12, d7 = 13;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7); // Make a object of LiquidCrystal and declare port of arduino to lcd

// Keypad Setup
const byte ROWS = 4;
const byte COLS = 3;  
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {22,24,26,28};  // pins of ROWS of Keypad
byte colPins[COLS] = {36,34,32};    // pins of COLS of Keypad  
Keypad kp = Keypad(makeKeymap(keys),rowPins,colPins,ROWS,COLS); //make and initialize a Keypad

// Other Pins
const int servo_pin = 7;
const int heatter_pin = 6;
const int humidifier_pin = 5;
const int buzzer_pin = 4;
const int humidity_pin = 3;
const int temperature_pin = A0;

// Menu setup
static void menuChanged(MenuChangeEvent changed);
static void menuUsed(MenuUseEvent used);

MenuBackend menu = MenuBackend(menuUsed,menuChanged);

MenuItem menuItem1 = MenuItem("Display");
  MenuItem menuItem11 = MenuItem("Temperature");
  MenuItem menuItem12 = MenuItem("Humidity");
  MenuItem menuItem13 = MenuItem("Status");
MenuItem menuItem2 = MenuItem("Account");
  MenuItem menuItem21 = MenuItem("Create Account");
  MenuItem menuItem22 = MenuItem("Change Password");
MenuItem menuItem3 = MenuItem("Exit");
MenuItem menuItem4 = MenuItem("Mode Setting");
  MenuItem menuItem41 = MenuItem("Setter");
    MenuItem menuItem411 = MenuItem("Temperature Of Setter");
    MenuItem menuItem412 = MenuItem("Humidity Of Setter");
    MenuItem menuItem413 = MenuItem("Turner Of Setter");
      MenuItem menuItem4131 = MenuItem("Active Setter");
      MenuItem menuItem4132 = MenuItem("Passive Setter");
    MenuItem menuItem414 = MenuItem("Time Of Setter");
  MenuItem menuItem42 = MenuItem("Hatcher");
    MenuItem menuItem421 = MenuItem("Temperature Of Hatcher");
    MenuItem menuItem422 = MenuItem("Humidity Of Hatcher");
    MenuItem menuItem423 = MenuItem("Turner Of Hatcher");
        MenuItem menuItem4231 = MenuItem("Active Hatcher");
        MenuItem menuItem4232 = MenuItem("Passive Hatcher");
    MenuItem menuItem424 = MenuItem("Time Of Hatcher");
MenuItem menuItem5 = MenuItem("Start Device");
MenuItem menuItem6 = MenuItem("Terminate Device");

Servo turner;

// Sensor Data
int temperature;
bool humidity;

// Temperature
int temperature_treshhold_setter;
int temperature_treshhold_hatcher;
int temperature_treshhold;
int temperature_limit;

// Humidity
bool humidity_treshhold_setter;
bool humidity_treshhold_hatcher;
bool humidity_treshhold;

// Turner
bool turner_active_setter;
bool turner_active_hatcher;
bool turner_active;
int turner_time_setter;
int turner_time_hatcher;
int turner_time;
int turner_angle;
bool turner_direction;

// Time Setter/Hatcher
int time_setter;
int time_hatcher;

// Accounts
#define MAXIM_ACCOUNTS 10
int account_counter = 0;
int username[MAXIM_ACCOUNTS];
int password[MAXIM_ACCOUNTS];
bool isExit;
bool logining;

// Mode Of Device
int mode; 
#define INACTIVE_MODE 0
#define SETTER_MODE 1
#define HATCHER_MODE 2

// Default Value
#define UPDATER_TIME 7*1000*1000
#define TEMPERATURE_LIMIT_DEFAULT 40

// Print two lines with lcd
void printDual(String str1, String str2){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(str1);
    lcd.setCursor(0,1);
    lcd.print(str2);  
}

// Handle keypad and LCD for getting input from user
// if isPassword set true, LCD shows only "*"
int getInput(bool isPassword = false){
  int input = 0;
  lcd.setCursor(0,1);
  while(true){
    char inp = kp.getKey();
    if(inp){
      if(inp=='*'){       // Enter
        lcd.clear();
        lcd.setCursor(0,0);
        return input;
      }
      else if(inp=='#'){  //Reset
        lcd.setCursor(0,1);
        lcd.print("                ");
        lcd.setCursor(0,1);
        input = 0;
      }
      else{
        input*=10;
        input+= inp-'0';
        if(isPassword){
          lcd.print('*');
        }
        else{
          lcd.print(inp); 
        }
      }
    }
  }
}

// Create new account with username : username_new and password : password_new
bool create_account(int username_new, int password_new){
  for(int i=0 ; i<account_counter ; i++){
    if(username[i] == username_new ){
      return false;
    }
  }
  if(account_counter==MAXIM_ACCOUNTS-1){
    return false;
  }
  
  username[account_counter] = username_new;
  password[account_counter] = password_new;
  account_counter++;
  return true;
}

// If password of user_input is password_input return true
bool check_password(int username_input, int password_input){
  for(int i=0 ; i<account_counter ; i++){
    if(username[i] == username_input && password[i] == password_input){
      return true;
    }
  }
  return false;
}

// Change password of user, from old_password to new_password
bool change_password(int user, int old_password, int new_password){
  if(check_password(user,old_password)){
    for(int i=0 ; i<account_counter ; i++){
      if(username[i] == user){
        password[i] = new_password;
        return true;
      }
    }
  }
  else{
    return false;
  }
}

// This function sets initial value and some configuration
void initialize(){
  create_account(0,123);
  mode = INACTIVE_MODE;
  isExit = true;
  logining = false;

  temperature = 40;
  humidity = HIGH;
  
  temperature_treshhold_setter = 37;
  temperature_treshhold_hatcher = 37;
  
  temperature_limit = TEMPERATURE_LIMIT_DEFAULT;
  
  turner_active_setter = HIGH;
  turner_active_hatcher = LOW;
  
  turner_time_setter = 11*1000*1000;
  turner_time_hatcher = 11*1000*1000;

  turner_angle = 45;
  turner_direction = true;
  
  time_setter = 73*1000*1000;
  time_hatcher = 73*1000*1000;
  
  humidity_treshhold_setter = LOW;
  humidity_treshhold_hatcher = HIGH;

  printDual("In The Name Of","God !");
  delay(1000);
  printDual("Hello!","");
}

// Setup! ,Setup LCD,pin moding and initialize
void setup() {
  Serial.begin(9600);
  
  lcd.begin(16,2);
  lcd.setCursor(0,0);

  menu.getRoot().add(menuItem1).addRight(menuItem2).addRight(menuItem3).addRight(menuItem4).addRight(menuItem5).addRight(menuItem6);
  menuItem1.add(menuItem11).addRight(menuItem12).addRight(menuItem13);
  menuItem2.add(menuItem21).addRight(menuItem22);
  menuItem4.add(menuItem41).addRight(menuItem42);
  menuItem41.add(menuItem411).addRight(menuItem412).addRight(menuItem413).addRight(menuItem414);
  menuItem42.add(menuItem421).addRight(menuItem422).addRight(menuItem423).addRight(menuItem424);
  menuItem413.add(menuItem4131).addRight(menuItem4132);
  menuItem423.add(menuItem4231).addRight(menuItem4232);

  turner.attach(servo_pin);
  pinMode(heatter_pin,OUTPUT);
  pinMode(buzzer_pin,OUTPUT);
  pinMode(humidifier_pin,OUTPUT);
  pinMode(humidity_pin,INPUT);
  
  initialize();
}

// Humidifier turns on
void humidifier_on(){
  digitalWrite(humidifier_pin,HIGH);
}

// Humidifier turns off
void humidifier_off(){
  digitalWrite(humidifier_pin,LOW);
}

// Hetter turns on
void heatter_on(){
  digitalWrite(heatter_pin,HIGH);
}

// Hetter turns off
void heatter_off(){
  digitalWrite(heatter_pin,LOW);  
}

// Buzzer buzzes
void buzz(){
  Serial.println("Buzz");
  for(int j=0 ; j<5 ; j++){
    for(int i=0 ; i<50000 ; i++){
      digitalWrite(buzzer_pin,HIGH);
    }
    for(int i=0 ; i<50000 ; i++){
      digitalWrite(buzzer_pin,LOW);
      //delay(2);
    }
  }
}

// Turner takes a turn
void turner_turn(){
  Serial.println("Turner Turn");
  if(turner_angle == 90){
    for( ; turner_angle>45 ; turner_angle--){
      turner.write(turner_angle);
   //   delay(1);
    }
    turner_direction = false;    
  }
  else if( turner_angle == 0){
    for( ; turner_angle<45 ; turner_angle++){
      turner.write(turner_angle);
     // delay(1);
    }
    turner_direction = true;
  }
  else{
    if(turner_direction == true){
       for( ; turner_angle<90 ; turner_angle++){
         turner.write(turner_angle);
       //  delay(1);
       } 
    }
    else{
       for( ; turner_angle>0 ; turner_angle--){
         turner.write(turner_angle);
       //  delay(1);
       }      
    }
  }
}

// Turner stops and returns to first state
void turner_stop(){
  if(turner_angle > 45){
    for( ; turner_angle>45 ; turner_angle--){
      turner.write(turner_angle);
    //  delay(1);
    }
    turner_direction = false;
  }
  else if(turner_angle <45){
    for( ; turner_angle<45 ; turner_angle++){
      turner.write(turner_angle);
     // delay(1);
    }
    turner_direction = true;
  }
}

// Update tempreture and make alert for improper temperature and using heatter 
void check_temperature(){
  Serial.println("Temperature");
  int raw_voltage = analogRead(temperature_pin);
  float milli_voltage = (raw_voltage * 3300.0) / 1024.0 ;
  float kelvin = milli_voltage / 10;
  float celsius = kelvin - 273.15;  
  temperature = celsius;
  if(temperature<temperature_limit){
    buzz();
  }
  
  if(temperature<temperature_treshhold){
    heatter_on();
  }
  else{
    heatter_off();
  }
}

// Update humidity and make alert for improper humidity and using humidifier
void check_humidity(){
  Serial.println("Humidity ");
  humidity = digitalRead(humidity_pin);
  if(humidity_treshhold == HIGH && humidity == LOW){
    humidifier_on();
  }
  else{
    humidifier_off();
  }
}

// Update tempearture and humidity
void updater(){
  Serial.println("Updater");
  check_temperature();
  check_humidity();    
}

// Show mode at now to LCD, if loging out
void show_mode(){
  if(isExit && !logining){
    if(mode == INACTIVE_MODE){
      printDual("State:","Inactive");
    }
    else if(mode == SETTER_MODE){
      printDual("State:","Setter");     
    }
    else if(mode == HATCHER_MODE){
      printDual("State:","Hatcher");
    }
    else{
      printDual("ERROR:","Mode error!");
    }
  }
}

// Changeing mode from setter to hatcher or vise versa
void change_mode(bool setter_to_hatcher){
  if(setter_to_hatcher){
    temperature_treshhold = temperature_treshhold_hatcher;
    humidity_treshhold = humidity_treshhold_hatcher;
    turner_active = turner_active_hatcher;
    turner_time = turner_time_hatcher;
    mode = HATCHER_MODE;
    show_mode();
  }
  else{
    temperature_treshhold = temperature_treshhold_setter;
    humidity_treshhold = humidity_treshhold_setter;
    turner_active = turner_active_setter;
    turner_time = turner_time_setter;
    mode = SETTER_MODE;
    show_mode();

  }
}

// Terminate devise and stop all of updater and modules
void terminate_device(){
  Serial.println("Terminate1");
  Timer1.detachInterrupt();
  Serial.println("Terminate2");
  Timer6.detachInterrupt();
  Serial.println("Terminate3");
  Timer7.detachInterrupt();
  turner_stop();
  mode = INACTIVE_MODE;
  show_mode();
}

// Hatcher mode starting
void hatcher(){
  Serial.println("Hatcher");
  Timer6.detachInterrupt();
  if(turner_active){
    Timer7.detachInterrupt();
    turner_stop();
  }
  change_mode(true);
  Timer6.attachInterrupt(terminate_device).start(time_hatcher);
  if(turner_active){
    Timer7.attachInterrupt(turner_turn).start(turner_time);
  }
}

// Setter mode starting
void setter(){
  Serial.println("Setter");
  if(turner_active){
    turner_stop();
  }
  change_mode(false);
  Timer6.attachInterrupt(hatcher).start(time_setter);
  if(turner_active){
    Timer7.attachInterrupt(turner_turn).start(turner_time);
  }
}

// Start device for a cycle 
void start_device(){
  Serial.println("Start Device");
  Timer1.attachInterrupt(updater).start(UPDATER_TIME);
  setter();
}

// Loop of program, using for handle menu
void loop() {
  char key = kp.getKey();
  if(key){
    if(isExit){
      if(logining){
          printDual("Username:","");
          int new_username = getInput();
          printDual("Password:","");
          int new_password = getInput(true);
          bool result = check_password(new_username,new_password);
          if(result){
            isExit = false;
            logining = false;
            menu.toRoot();
          }
          else{
            isExit = true;
            logining = false;
            show_mode();
          }
      }
      else{
        if(key=='*'){
          logining = true;
        }
      }
    }
    else{
      MenuItem currentMenu = menu.getCurrent();
      if(key=='6'){
        menu.moveRight();
      }
      else if(key == '4' ){
        menu.moveLeft();
      }
      else if(key == '2'){
        menu.moveDown();
      }
      else if(key == '5'){
        menu.use();
      }
      else if(key =='8'){
        menu.moveUp();
      }
      else if(key =='#'){
        menu.toRoot();
      }    
    }
  } 
}

// menuChankeed function from "MenuBackend.h", when change menu Item calls
void menuChanged(MenuChangeEvent changed){
  MenuItem newMenu = changed.to;
  lcd.setCursor(0,0);
  if(newMenu == menu.getRoot()){
    lcd.clear();
    lcd.print("Menu");
  }
  else{
    lcd.clear();
    lcd.print(newMenu.getName()); 
  }
}

// menuUsed function from "MenuBackend.h", when use a menu Item
void menuUsed(MenuUseEvent used){

  if(used.item.getName() == "Temperature"){
    printDual("Temperature:",String(temperature));
  }
  else if(used.item.getName() == "Humidity"){
    if(humidity == HIGH){
      printDual("Humidity:","HIGH");
    }
    else{
      printDual("Humidity:","LOW");
    }
  }
  else if(used.item.getName() == "Status"){
    printDual("Status:",String(turner_angle));
  }
  else if(used.item.getName() == "Create Account"){
    printDual("Account Id:","");
    int new_username = getInput();
    printDual("Password:","");
    int new_password = getInput(true);
    
    int result = create_account(new_username,new_password);
    if(result==true){
      printDual("","Account Created!");
    }
    else{
      printDual("Error:","Create Failed!");
    }
  }
  else if(used.item.getName() == "Change Password"){
    printDual("Account Id:","");
    int current_username = getInput();
    printDual("Old password:","");
    int old_password = getInput(true);
    printDual("New password:","");
    int new_password = getInput(true);
    
    
    int result = change_password(current_username,old_password,new_password);
    if(result==true){
      printDual("","Change Done!");
    }
    else{
      printDual("Error:","Change Failed!");
    }    
  }
  else if(used.item.getName() == "Exit"){
    isExit = true;
    logining = false;
    show_mode();
  }
  else if(used.item.getName() == "Temp Of Setter"){
    printDual("Temperature:","");
    temperature_treshhold_setter = getInput();
    printDual("","Done!");
  }
  else if(used.item.getName() == "Humidity Of Setter"){
    printDual("Humidity:(0/1)","");
    int result = getInput();
    if(result==0){
      printDual("Passive","Done!");      
      humidity_treshhold_setter = LOW;
    }
    else if(result==1){
      printDual("Acctive","Done!");            
      humidity_treshhold_setter = HIGH;
    }
    else{
      printDual("Error:","Change Failed!");
    }
    
  }
  else if(used.item.getName() == "Active Setter"){
    turner_active_setter = HIGH;
    printDual("Time Turner:","");
    turner_time_setter = getInput();
  }
  else if(used.item.getName() == "Passive Setter"){
    turner_active_setter = LOW;
    printDual("Passive","Done!");
  }
  else if(used.item.getName() == "Time Of Setter"){
    printDual("Time:","");
    time_setter = getInput();
  }
  else if(used.item.getName() == "Temp Of Hatcher"){
    printDual("Temperature:","");
    temperature_treshhold_hatcher = getInput();
    printDual("","Done!");    
  }
  else if(used.item.getName() == "Humidity Of Hatcher"){
    printDual("Humidity:(0/1)","");
    int result = getInput();
    if(result==0){
      printDual("Passive","Done!");      
      humidity_treshhold_hatcher = LOW;
    }
    else if(result==1){
      printDual("Acctive","Done!");            
      humidity_treshhold_hatcher = HIGH;
    }
    else{
      printDual("Error:","Change Failed!");
    }    
  }
  else if(used.item.getName() == "Active Hatcher"){
    turner_active_hatcher = HIGH;
    printDual("Time Turner:","");
    turner_time_hatcher = getInput();    
  }
  else if(used.item.getName() == "Passive Hatcher"){
    turner_active_hatcher = LOW;
    printDual("Passive","Done!");
  }
  else if(used.item.getName() == "Time Of Hatcher"){
    printDual("Time:","");
    time_hatcher = getInput();    
  }
  else if(used.item.getName() == "Start Device"){
    printDual("Start Device...","3");
    delay(500);
    printDual("Start Device...","2");
    delay(500);
    printDual("Start Device...","1");
    delay(500);
    start_device();
    
  }
  else if(used.item.getName() == "Terminate Device"){
    printDual("Terminate Device...","3");
    delay(500);
    printDual("Terminate Device...","2");
    delay(500);
    printDual("Terminate Device...","1");
    delay(500);    
    terminate_device();
  }
}
